<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
include_spip('inc/documents');

function formulaires_editer_diagramme_charger_dist(
	$id_document = 'new',
	$id_objet = 0,
	$objet = ''
) {
	$valeurs = array();
	if ($id_document != 'new'){
		$fichier = sql_getfetsel('fichier', 'spip_documents', 'id_document=' . intval($id_document));
		if($fichier){
			$json_content = json_decode(file_get_contents(get_spip_doc($fichier)), true);
			$json_content['hotdata'] = json_encode($json_content['hotdata']);
			$valeurs += $json_content;
		}
	}
	$valeurs['objet'] = $objet;
	$valeurs['id_objet'] = $id_objet;
	$valeurs['id_document'] = $id_document;
	return $valeurs;
}

function formulaires_editer_diagramme_verifier_dist(
	$id_document = 'new',
	$id_objet = 0,
	$objet = ''
) {
	$erreurs = array();
	return $erreurs;
}

function formulaires_editer_diagramme_traiter_dist(
	$id_document = 'new',
	$id_objet = 0,
	$objet = ''
) {
	$res = array();
	$json_content = array();
	$ajouter_documents = charger_fonction('ajouter_documents', 'action');
	foreach($_POST as $key => $value){
		if($key == 'json_hotdata') $value = json_decode($value);
		if($key == 'json_custom_colors' || $key == 'json_units'){ 
			if(trim($value) == "") unset($_POST[$key]);
			else $value = explode('|', $value);
		}
		if(isset($_POST[$key]) && substr($key, 0, 5) == 'json_') $json_content[substr($key, 5)] = $value;
	}
	if ($id_document == 'new'){
		file_put_contents(_DIR_TMP."diagramme_tmp.json", json_encode($json_content));
		$file = array('tmp_name' => _DIR_TMP."diagramme_tmp.json", 'name' => 'diagramme.json');
		$files = array($file);
		// if Json not in allowed extensions list, we add it
		//$row = sql_getfetsel('*', 'spip_types_documents', 'extension="json"');
		//if(!$row) sql_insertq('spip_types_documents',array('titre' => 'JSON', 'extension' => 'json', 'mime_type' => 'application/json', 'upload' => 'oui'));
		if($x = $ajouter_documents('new', $files, $objet, $id_objet, 'document')){ 
			$res['message_ok'] = _T("diagrammes:chart_added");
			$new_id_document = $x[0];
			sql_updateq('spip_documents', array('titre' => _request("json_title"), 'media' => 'diagramme'), "id_document=".intval($new_id_document));
		}
		unlink(_DIR_TMP."diagramme_tmp.json");
	}
	else{
		$fichier = sql_getfetsel('fichier', 'spip_documents', 'id_document=' . intval($id_document));
		if($fichier){
			if(file_put_contents(get_spip_doc($fichier), json_encode($json_content))){ 
				sql_updateq('spip_documents', array('titre' => _request("json_title")), "id_document=".intval($id_document));
				$res['message_ok'] = _T("diagrammes:chart_updated");
			}
		}
	}
	if($id_document == 'new') $id_parent = $new_id_document;
	else $id_parent = $id_document;
	$id_vignette = 0;
	// if there is an image, we make it the thumbnail
	if(isset($_POST['spip_thumb']) && trim($_POST['spip_thumb']) != ''){
		// split the string on commas
    // $data[ 0 ] == "data:image/png;base64"
    // $data[ 1 ] == <actual base64 string>
    $data = explode( ',', $_POST['spip_thumb'] );
    file_put_contents(_DIR_TMP."diagramme_thumb_tmp.jpg", base64_decode($data[1]));
    //vignette already exists?
		$create_vignette = true;
		if($id_document != 'new'){
			$res_vignette = sql_select("d1.id_document", "spip_documents d1, spip_documents d2", "d2.id_document=".intval($id_parent)." and d2.id_vignette=d1.id_document", "", "", 1);
			if($row = spip_fetch_array($res_vignette)){ 
				$id_vignette = $row["id_document"];
				$create_vignette = false;
			}
		}
    $file = array('tmp_name' => _DIR_TMP."diagramme_thumb_tmp.jpg", 'name' => 'diagramme_thumb.jpg');
		$files = array($file);
		$ajoute = $ajouter_documents($id_vignette, $files, '', 0, 'vignette');
		if(is_numeric(reset($ajoute)) and $id_vignette = reset($ajoute)){
			include_spip('action/editer_document');
			document_modifier($id_parent, array('id_vignette' => $id_vignette, 'mode' => 'document'));
		}
		unlink(_DIR_TMP."diagramme_thumb_tmp.jpg");
	}
	if($id_document == 'new'){
		$callback .= "jQuery('#doc".$new_id_document." a.editbox').eq(0).focus();";
		$js = "if (window.jQuery) jQuery(function(){ajaxReload('documents',{callback:function(){ $callback }});});";
		$js = "<script type='text/javascript'>$js</script>";
	}
	else{
		$js = '<script type="text/javascript">if (window.jQuery) ajaxReload("document_infos");</script>';
		$js .= "<script type='text/javascript'>if (window.jQuery) jQuery.modalboxclose();</script>";
	}
	if ($res['message_ok']) {
		 $res['message_ok'] .= $js;
	}
	$res['editable'] = 1;
	return $res;
}