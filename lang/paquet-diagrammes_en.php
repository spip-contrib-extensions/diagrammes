<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'diagrammes_description' => 'Create charts with Javascript.',
	'diagrammes_nom' => 'Easy charts',
	'diagrammes_slogan' => 'Allows to use charts as documents',
);

?>