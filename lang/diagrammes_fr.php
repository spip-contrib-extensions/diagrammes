<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(

		// A
		'angle_line_color' => 'Couleur des lignes (ex: "#888", "rgba(0,0,0,0.1)"): ',
		'angle_line_width' => 'Taille des lignes: ',
		'arc_border_width' => 'Bordure des arcs: ',
		'area_background_color' => 'Couleur de fond du graphe (ex: #FFF): ',
		'aspect_ratio' => 'Ratio largeur/hauteur: ',
		'axes' => 'Axes',
		'axis' => 'Axe',
		
		// B
		'background_color' => 'Couleur de fond du canva (ex: #FFF): ',
		'background_color_opacity' => 'Opacit&eacute; du remplissage: ',
		'bar_percentage' => 'Pourcentage des barres: ',
		'begin at zero' => 'Commencer &agrave; z&eacute;ro',
		'border_color' => 'Couleur de bordures: ',	
		'border_radius' => 'Espace de bordures: ',
		'border_width' => '&Eacute;paisseur des bordures: ',
		
		// C
		'category_percentage' => 'Pourcentage des cat&eacute;gories: ',
		'chart_added' => 'Graphe cr&eacute;&eacute; avec succ&egrave;s!',
		'chart_title' => 'Titre',
		'chart_type' => 'Type de graphe : ',
		'chart_type_bar' => 'Colonnes',
		'chart_type_bubble' => 'Bulles',
		'chart_type_doughnut' => 'Doughnut',
		'chart_type_horizontalbar' => 'Barres horizontales',
		'chart_type_line' => 'Courbe',
		'chart_type_pie' => 'Camembert',
		'chart_type_polararea' => 'Diagramme polaire',
		'chart_type_radar' => 'Radar',
		'chart_type_scatter' => 'Nuage de points',
		'chart_updated' => 'Graphe mis &agrave; jour!',
		'circle' => 'Cercle',
		'circumference' => 'Circonf&eacute;rence: ',
		'color_ex' => 'Couleur (ex: "#888", "rgba(0,0,0,0.1)"): ',
		'columns' => 'Colonnes',
		'configurer_titre' => 'Configurer diagrammes &eacute;ditables',
		'create_chart' => 'Cr&eacute;&eacute; Graphe',
		'cross' => 'Croix',
		'cross_rot' => 'Croix Rot',
		'csv_import_export' => 'CSV Import/Export',
		'cubic_interpolation' => 'Interpolation cubique: ',
		'custom_colors' => 'Couleurs personalis&eacute;es',
		'custom_colors_explaination' => 'Vous pouvez sp&eacute;cifier des couleurs personalis&eacute;es pour les lignes du graphe, en code hexa et s&eacute;par&eacute;es par une barre "|" (ex: #99d8c9|#c994c7|etc.)',
		'cut_out_percentage' => 'Pourcentage coup&eacute;: ',
		
		// D
		'dash' => 'Tiret',
		'data' => 'Donn&eacute;es',
		'datalabels' => 'Data Labels',
		'datalabels_position' => 'Position des data labels',
		'default' => 'D&eacute;faut',
    'deferred_active' => '-Voulez-vous activer le plugin \'deferred\' (chargement diff&eacute;r&eacute;)?',
    'deferred_calibration_explaination' => 'X offset est la partie horizontale minimum du canva qui doit etre visible pour commencer a dessiner le graphe.<br>Y offset est la partie verticale minimum du canva qui doit etre visible pour commencer a dessiner le graphe. Vous pouvez utiliser des entiers (qui seront interpretes en pixels) ou des pourcentages.<br><br>Exemples: 150,-150,30%<br><br>Le delai est le temps en millisecondes que le plugin attendra avant de dessiner le graphe, apres que les conditions d\'offsets X/Y soient remplies.',
    'deferred_calibration_title' => '-Calibrage',
    'deferred_delay' => 'D&eacute;lai:',
    'deferred_explaination' => 'Ce plugin va permettre de dessiner les graphes uniquement quand ils atteignent un certain niveau de la fen&ecirc;tre d\'affichage, permettant aux visiteurs de voir les animations des graphes.',
    'deferred_title' => 'Plugin deferred (diff&eacute;r&eacute;)',
    'deferred_xoffset' => 'X offset:',
    'deferred_yoffset' => 'Y offset:',
    'display_angle_lines' => 'Afficher les lignes',
    'display_axis' => 'Afficher l\'axe',
    'display_datalabels' => 'Afficher data labels',
    'display_labels' => 'Afficher les labels',
    'display_legend' => 'Afficher la l&eacute;gende',
    'display_ticks' => 'Afficher les puces',
    'display_title' => 'Afficher le titre',
    
    // E
    'editer_chart' => 'Editer graphe',
    'editer_chart' => 'Editer graphe',
    'export' => 'Exporter',
    'extra' => 'Extra',
    
    // F
    'font_color' => 'Couleur de police (ex: #888): ',
    'font_family' => 'Famille de police: ',
    'font_size' => 'Taille de police: ',
    'font_style' => 'Style de police: ',
    
    // H
    'height_px' => 'Hauteur(px): ',
    
    // I
    'icone_creer_chart' => 'Cr&eacute;er graphe',
    'import' => 'Importer un fichier',
    'import_error' => 'Une erreur s\'est produite.',
    
    // L
    'labels' => 'Labels',
    'layout' => 'Pr&eacute;sentation',
    'legend' => 'L&eacute;gende',
    'legend_box_width' => 'Taille des boites: ',
    'legend_position' => 'Position de la l&eacute;gende: ',
    'line' => 'Courbe',
    'line_border_width' => 'Epaisseur de la courbe: ',
    'line_height' => 'Hauteur de ligne: ',
    'line_options' => 'Options de courbe/zones',
    'line_tension' => 'Tension de la courbe: ',
    
    // M
    'maintain_aspect_ratio' => 'Maintenir le ratio',
    'max_height_px' => 'Hauteur max(px): ',
    'max_width_px' => 'Largeur max(px): ',
    'monotone' => 'Monotone',
    
    // O
    'offset' => 'D&eacute;calage: ',
    
    // P
    'padding' => 'Espacement: ',
    'padding_bottom' => 'Espacement bas: ',
    'padding_left' => 'Espacement gauche: ',
    'padding_right' => 'Espacement droit: ',
    'padding_top' => 'Espacement haut: ',
    'parse_data_in' => 'Parser les donn&eacute;es selon: ',
    'point_border_width' => 'Epaisseur des points: ',
    'point_hover_radius' => 'Largeur des points survol&eacute;s: ',
    'point_radius' => 'Largeur des points: ',
    'point_style' => 'Style des points: ',
    'position' => 'Position: ',
    'position_bottom' => 'Bas',
    'position_center' => 'Centre',
    'position_left' => 'Gauche',
    'position_right' => 'Droite',
    'position_top' => 'Haut',
    'preview' => 'Aper&ccedil;u',
    
    // R
    'rect' => 'Rect',
    'rect_rot' => 'Rect Rot',
    'rect_rounded' => 'Rect Arondi',
    'responsive' => 'Responsive',
    'reverse_legend' => 'Inverser la l&eacute;gende',
    'rotation' => 'Rotation: ',
    'rows' => 'Lignes',
    
    // S
    'show_lines' => 'Afficher les lignes',
    'stacked' => 'Superpos&eacute;',
    'star' => 'Etoile',
    'start_angle' => 'Angle de d&eacute;part: ',
    
    // T
    'ticks' => 'Puces',
    'title' => 'Titre: ',
    'title_position' => 'Position du titre: ',
    'tooltip_units' => 'Unit&eacute;s dans les infobulles (ex: USD, visiteurs, etc.): ',
    'triangle' => 'Triangle', 
    
    // W
    'width_px' => 'Largeur(px): ',
    
    // X
    'xaxis' => 'Axe X',
    
    // Y
    'yaxis' => 'Axe Y'
);

?>