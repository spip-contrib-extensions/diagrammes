<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(
		// A
		'angle_line_color' => 'Angle line color (ex: "#888", "rgba(0,0,0,0.1)"): ',
		'angle_line_width' => 'Angle line width: ',
		'arc_border_width' => 'Arc border width: ',
		'area_background_color' => 'Chart area background color (ex: #FFF): ',
		'aspect_ratio' => 'Aspect ratio: ',
		'axes' => 'Axes',
		'axis' => 'Axis',
		
		// B
		'background_color' => 'Background color (ex: #FFF): ',
		'background_color_opacity' => 'Background color opacity: ',
		'bar_options' => 'Bar options',
		'bar_percentage' => 'Bar percentage: ',
		'begin at zero' => 'Begin at zero',
		'border_color' => 'Border color: ',
		'border_radius' => 'Border radius: ',
		'border_width' => 'Border width: ',
		
		// C
		'category_percentage' => 'Category percentage: ',
		'chart_added' => 'Chart successfully added!',
		'chart_title' => 'Title',
		'chart_type' => 'Chart type : ',
		'chart_type_bar' => 'Column',
		'chart_type_bubble' => 'Bubble',
		'chart_type_doughnut' => 'Doughnut',
		'chart_type_horizontalbar' => 'Bar',
		'chart_type_line' => 'Line',
		'chart_type_pie' => 'Pie',
		'chart_type_polararea' => 'Polar Area',
		'chart_type_radar' => 'Radar',
		'chart_type_scatter' => 'Scatter',
		'chart_updated' => 'Chart successfully updated!',
		'circle' => 'Circle',
		'circumference' => 'Circumference: ',
		'color_ex' => 'Color (ex: "#888", "rgba(0,0,0,0.1)"): ',
		'columns' => 'Columns',
		'configurer_titre' => 'Easy charts setup',
		'create_chart' => 'Create chart',
		'cross' => 'Cross',
		'cross_rot' => 'Cross Rot',
		'csv_import_export' => 'CSV Import/Export',
		'cubic_interpolation' => 'Cubic interpolation: ',
		'custom_colors' => 'Custom colors',
		'custom_colors_explaination' => 'You can specify your own custom colors to be used in the graph, in hex code and separated by a pipe sign "|" (ex: #99d8c9|#c994c7|etc.)',
		'cut_out_percentage' => 'Cut out percentage: ',
		
		// D
		'dash' => 'Dash',
		'data' => 'Data',
		'datalabels' => 'Data Labels',
		'datalabels_position' => 'Data labels position',
		'default' => 'Default',
		'deferred_active' => '-Do you want to use the deferred plugin?',
    'deferred_calibration_explaination' => 'X offset is the horizontal minimum amount of the canva that must be visible on the viewport to start drawing the chart.<br>Y offset is the vertical minimum amount of the canva that must be visible on the viewport to start drawing the chart. You can use integers (interpreted in pixels) or percentages.<br><br>Examples: 150,-150,30%<br><br>The delay is the time in milliseconds we will wait to start drawing the chart, starting when the X/Y offset conditions are met.',
    'deferred_calibration_title' => '-Calibration',
    'deferred_delay' => 'Delay:',
    'deferred_explaination' => 'This plugin will load the charts only when they are visible in the viewport, trigerring the chart animation only when it is visible from the visitor.',
    'deferred_title' => 'Deferred plugin',
    'deferred_xoffset' => 'X offset:',
    'deferred_yoffset' => 'Y offset:',
    'display_angle_lines' => 'Display angle lines',
    'display_axis' => 'Display axis',
    'display_datalabels' => 'Display data labels',
    'display_labels' => 'Display labels',
    'display_legend' => 'Display legend',
    'display_ticks' => 'Display ticks',
    'display_title' => 'Display title',
    
    // E
    'editer_chart' => 'Edit chart',
    'export' => 'Export',
    'extra' => 'Extra',
    
    // F
    'font_color' => 'Font Color (ex: #888): ',
    'font_family' => 'Font Family: ',
    'font_size' => 'Font size: ',
    'font_style' => 'Font style: ',
    
    // H
    'height_px' => 'Height(px): ',
    
    // I
    'icone_creer_chart' => 'Create chart',
    'import' => 'Import a file',
    'import_error' => 'Sorry there has been an error.',
    
    // L
    'labels' => 'Labels',
    'layout' => 'Layout',
    'legend' => 'Legend',
    'legend_box_width' => 'Box width: ',
    'legend_position' => 'Legend position: ',
    'line' => 'Line',
    'line_border_width' => 'Line border width: ',
    'line_height' => 'Line height: ',
    'line_options' => 'Line options',
    'line_tension' => 'Line tension: ',
    
    // M
    'maintain_aspect_ratio' => 'Maintain aspect ratio',
    'max_height_px' => 'Max Height(px): ',
    'max_width_px' => 'Max Width(px): ',
    'monotone' => 'Monotone',
    
    // O
    'offset' => 'Offset: ',
    
    // P
    'padding' => 'Padding: ',
    'padding_bottom' => 'Padding bottom: ',
    'padding_left' => 'Padding left: ',
    'padding_right' => 'Padding right: ',
    'padding_top' => 'Padding top: ',
    'parse_data_in' => 'Parse data in: ',
    'point_border_width' => 'Point border width: ',
    'point_hover_radius' => 'Point hover radius: ',
    'point_radius' => 'Point radius: ',
    'point_style' => 'Point style: ',
    'position' => 'Position: ',
    'position_bottom' => 'Bottom',
    'position_center' => 'Center',
    'position_left' => 'Left',
    'position_right' => 'Right',
    'position_top' => 'Top',
    'preview' => 'Preview',
    
    // R
    'rect' => 'Rect',
    'rect_rot' => 'Rect Rot',
    'rect_rounded' => 'Rect Rounded',
    'responsive' => 'Responsive',
    'reverse_legend' => 'Reverse Legend',
    'rotation' => 'Rotation: ',
    'rows' => 'Rows',
    
    // S
    'show_lines' => 'Show lines',
    'stacked' => 'Stacked',
    'star' => 'Star',
    'start_angle' => 'Start angle: ',
    
    // T
    'ticks' => 'Ticks',
    'title' => 'Title: ',
    'title_position' => 'Title position: ',
    'tooltip_units' => 'Tooltip units (ex: USD, visitors, etc.): ',
    'triangle' => 'Triangle', 
    
    // W
    'width_px' => 'Width(px): ',
    
    // X
    'xaxis' => 'X Axis',
    
    // Y
    'yaxis' => 'Y Axis'
);

?>