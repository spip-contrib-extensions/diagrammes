<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'diagrammes_description' => 'Cr&eacute;er des graphiques en javascript.',
	'diagrammes_nom' => 'Diagrammes &eacute;ditables',
	'diagrammes_slogan' => 'Permet d\'utiliser des graphes Javascript comme des documents',
);

?>