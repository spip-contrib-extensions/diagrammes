var chartJsColorList = ["#0099ed","#f98108","#dc1e30","#4caf50","#607d8b","#69D2E7","#C994C7","#96CE7F","#CEBC17","#CE4264","#F7464A","#46BFBD","#FDB45C","#FD7B9B","#8C99FD","#A2DB2F","#DB7368","#6B84DB","#DBBF37","#DB97AB","#2EC087"];
var chartJsBackgroundColor = "#FFF";
var chartJsChartAreaBackgroundColor = "#FFF";

class SpipChart{
	constructor(chartId, chartOptions) {
      this.chartId = chartId;
      if(typeof chartOptions.chartJsOptions !== 'undefined'){ 
      	this.chartOptions = chartOptions.chartJsOptions;
      	if(typeof chartOptions.chartJsOptions.type !== 'undefined') this.chartType = chartOptions.chartJsOptions.type;
      }
      if(typeof chartOptions.opacity !== 'undefined') this.opacity = chartOptions.opacity;
      else this.opacity = 1;
      if(typeof chartOptions.borderWidth !== 'undefined') this.borderWidth = chartOptions.borderWidth;
      else this.borderWidth = 3;
      if(typeof chartOptions.colorList !== 'undefined') this.colorList = chartOptions.colorList;
      else this.colorList = chartJsColorList;
      if(typeof chartOptions.backgroundColor !== 'undefined') this.backgroundColor = chartOptions.backgroundColor;
      else this.backgroundColor = chartJsBackgroundColor;
      if(typeof chartOptions.chartAreaBackgroundColor !== 'undefined') this.chartAreaBackgroundColor = chartOptions.chartAreaBackgroundColor;
      else this.chartAreaBackgroundColor = chartJsChartAreaBackgroundColor;
      if(typeof chartOptions.units !== 'undefined') this.units = chartOptions.units; 
      else this.units = [];   
  }
	
	// build chartjs object
	build(viewMode){
    // destroy current chart if any
    if(typeof this.chart !== "undefined") this.chart.destroy();
    
    // preparing colors into the dataset
    this.insert_chart_colors();
  
    // preparing tooltip units
    if(this.units.length > 0) this.update_tooltip_units();
      
    // preparing canvas background
    this.set_backgrounds();
    
    this.chart = new Chart(document.querySelector('.chartJS'+viewMode+'Container'+this.chartId).getContext('2d'), this.chartOptions);
    this.update_chart_units();
    console.log(this);
  }    
  
	// add colors to a chart datasets
	insert_chart_colors(){
		if(this.chartType == "pie" || this.chartType == "doughnut" || this.chartType == "polarArea"){ 
			if(typeof this.chartOptions.data.datasets[0].backgroundColor === 'undefined') this.chartOptions.data.datasets[0].backgroundColor = [];
			if(this.chartType == "polarArea" && typeof this.chartOptions.data.datasets[0].borderColor === 'undefined') this.chartOptions.data.datasets[0].borderColor = [];
			for(var i = 0; i < this.chartOptions.data.datasets[0].data.length; i++){
		  	if(typeof this.colorList[i] !== 'undefined' && is_valid_hex(this.colorList[i])){
		  		if(this.chartType == "polarArea") this.chartOptions.data.datasets[0].borderColor[i] = chartjs_color_to_rgb(this.colorList[i], 1); 
		  		this.chartOptions.data.datasets[0].backgroundColor[i] = chartjs_color_to_rgb(this.colorList[i], this.opacity);
				}
			}
		}
		else{
			for(var i = 0; i < this.chartOptions.data.datasets.length; i++){
		  	if(typeof this.colorList[i] !== 'undefined' && is_valid_hex(this.colorList[i])){
		  		this.chartOptions.data.datasets[i].backgroundColor = chartjs_color_to_rgb(this.colorList[i], this.opacity);
		  		this.chartOptions.data.datasets[i].borderColor = chartjs_color_to_rgb(this.colorList[i], 1);
		  		this.chartOptions.data.datasets[i].borderWidth = this.borderWidth;
		  	}
		  }
		}
	}
	
	// set background colors for chart (canvas & chart area)
	set_backgrounds(){
		var bkgColor = this.backgroundColor;
		var chartAreaBkgColor = this.chartAreaBackgroundColor;
		this.chartOptions.plugins = [{
			beforeDraw : function(chartInstance, options){
		  	var ctx = chartInstance.chart.ctx;
			  ctx.fillStyle = bkgColor;
			  ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
			  ctx.fillStyle = chartAreaBkgColor;
			  var chartArea = chartInstance.chartArea;
	  		ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);    
		  }
		}];
	}
	
	// make custom units set in options available to tooltips callbacks by placing them in data property
	update_chart_units(){
		if(typeof this.chart.options.units !== 'undefined'){
			this.chart.config.data.units = this.chart.options.units;
			for(var i = 0; i < this.chart.data.datasets.length; i++){
				if(typeof this.chart.options.units === "string") this.chart.data.datasets[i].unit = this.chart.options.units;
				else if(typeof this.chart.options.units[i] !== "undefined") this.chart.data.datasets[i].unit = this.chart.options.units[i];
			}
		}
	}
	
	
	// arm tooltips to show custom units
	update_tooltip_units(){
		if(["pie", "doughnut", "polarArea"].indexOf(this.chartType) > -1){
			this.chartOptions.options.tooltips = {
				callbacks : {
					label : function(tooltipItem, data) {
	    			if(typeof data.units[tooltipItem.index] !== "undefined") return data.labels[tooltipItem.index]+": "+data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]+' '+data.units[tooltipItem.index];
	    			else return data.labels[tooltipItem.index]+": "+data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	    		}
	    	}
	    };
		}
		else if(["radar"].indexOf(this.chartType) > -1){
			this.chartOptions.options.tooltips = {
				callbacks : {
					label : function(tooltipItem, data) {
	    			if(typeof data.units[tooltipItem.index] !== "undefined") return data.datasets[tooltipItem.datasetIndex].label+': '+tooltipItem.yLabel+' '+data.units[tooltipItem.index];
	    			else return data.datasets[tooltipItem.datasetIndex].label+": "+tooltipItem.yLabel;
	    		}
	    	}
	    };
		}
		else if(["horizontalBar"].indexOf(this.chartType) > -1){
			this.chartOptions.options.tooltips = {
				callbacks : {
					label : function(tooltipItem, data) {
	    			if(typeof data.datasets[tooltipItem.datasetIndex].unit !== "undefined") return data.datasets[tooltipItem.datasetIndex].label+": "+tooltipItem.xLabel+' '+data.datasets[tooltipItem.datasetIndex].unit;
	    			else return data.datasets[tooltipItem.datasetIndex].label+": "+tooltipItem.xLabel;
	    		}
	    	}
	    };
		}
		else if(["line", "bar"].indexOf(this.chartType) > -1){
			this.chartOptions.options.tooltips = {
				callbacks : {
					label : function(tooltipItem, data) {
	    			if(typeof data.datasets[tooltipItem.datasetIndex].unit !== "undefined") return data.datasets[tooltipItem.datasetIndex].label+": "+tooltipItem.yLabel+' '+data.datasets[tooltipItem.datasetIndex].unit;
	    			else return data.datasets[tooltipItem.datasetIndex].label+": "+tooltipItem.yLabel;
	    		}
	    	}
	    };
		}
	}
	
}

function chartjs_color_to_rgb(color, opacity){
	res = hexToRgb(color);
	return "rgba("+res.r+", "+res.g+", "+res.b+", "+opacity+")";
}

function hexToRgb(hex) {
    if(hex.length == 3) hex = hex+hex;
    if(hex.length == 4) hex = "#"+hex.substr(1, 3)+hex.substr(1, 3)
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

function is_valid_hex(hex){
	var isOk  = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(hex);
	return isOk;
}

function escapeSpecialChars(jsonString) {
  return jsonString.replace(/\n/g, "\\n")
      .replace(/\r/g, "\\r")
      .replace(/\t/g, "\\t")
      .replace(/\f/g, "\\f");
}