class SpipChartPrive extends SpipChart{
	
	constructor(chartId, chartOptions, hotConfig) {
		super(chartId, chartOptions);
		this.parsing = chartOptions.parsing;
		// we need to init HoT here ...
		this.hotConfig = hotConfig;
		this.set_hooks(); 
		
		this.hot = new Handsontable (document.getElementById("formtable"+this.chartId), this.hotConfig);
		//this.build();
		this.refresh_chart("refresh", false);
		
		// we need a pause to load HOT correctly in modal popup
		setTimeout(function(){window["spipChartPrive"+chartId].hot.render();},500);
		
	}
	
	// refresh the chart by reading the form and the HoT data
	// 3 types of refresh modes: 
	// - Update -> change an existing property of the chart
	// - Redraw -> we need to re-read the HoT and update the dataset
	// - Refresh -> the structure of the chart has changed with more or less properties than before, we destroy and rebuild it
	refresh_chart(refreshMode, changeType){
		// first it is important to get the new type
		this.chartType = document.querySelector("#chartForm"+this.chartId+" select[name=\"json_type\"]").value;
		document.querySelector("#display"+this.chartId).className = this.chartType+"-chart";
		
		/* annoying specifics when we switch type of chart */
		if(changeType){ 
			// some charts prefer a background color with full opacity as default
			if(["pie", "doughnut", "bar", "horizontalBar"].includes(this.chartType)) document.querySelector("#chartForm"+this.chartId+" select[name=\"json_background_color_opacity\"]").value = 1;
			else document.querySelector("#chartForm"+this.chartId+" select[name=\"json_background_color_opacity\"]").value = 0.2;
			// if we switch to radar
			if(this.chartType == "radar"){ 
				// we start with a tension of 0 to avoid scaring the user
				document.querySelector("#chartForm"+this.chartId+" select[name=\"json_line_tension\"]").value = 0;
				// we show the labels
				document.querySelector("#chartForm"+this.chartId+" input[name=\"json_axes_point_labels_display\"][type=checkbox]").checked = true;
			}
			// if we switch to polarArea, we make sure labels are hidden
			if(this.chartType == "polarArea") document.querySelector("#chartForm"+this.chartId+" input[name=\"json_axes_point_labels_display\"][type=checkbox]").checked = false;
		}
		
		// get all the new values if updated
		this.parsing = document.querySelector("#chartForm"+this.chartId+" select[name=\"json_parsing\"]").value;
		this.opacity = document.querySelector("#chartForm"+this.chartId+" select[name=\"json_background_color_opacity\"]").value;
    this.borderWidth = document.querySelector("#chartForm"+this.chartId+" select[name=\"json_line_border_width\"]").value;;
    if(document.querySelector("#chartForm"+this.chartId+" textarea[name=\"json_custom_colors\"]").value.trim() !== '') this.colorList = document.querySelector("#chartForm"+this.chartId+" textarea[name=\"json_custom_colors\"]").value.trim().split("|");
    else this.colorList = chartJsColorList;
    if(document.querySelector("#chartForm"+this.chartId+" input[name=\"json_background_color\"]").value.trim() !== '') this.backgroundColor = document.querySelector("#chartForm"+this.chartId+" input[name=\"json_background_color\"]").value.trim();
    else this.backgroundColor = chartJsBackgroundColor;
    if(document.querySelector("#chartForm"+this.chartId+" input[name=\"json_chart_area_background_color\"]").value.trim() !== '') this.chartAreaBackgroundColor = document.querySelector("#chartForm"+this.chartId+" input[name=\"json_chart_area_background_color\"]").value.trim();
    else this.chartAreaBackgroundColor = chartJsChartAreaBackgroundColor;
    if(document.querySelector("#chartForm"+this.chartId+" input[name=\"json_units\"]").value.trim() !== '') this.units = document.querySelector("#chartForm"+this.chartId+" input[name=\"json_units\"]").value.trim(); 
    else this.units = [];   
		this.hotData = this.hot.getSourceData();
		
		// special process to add/remove extra Y axes in the form, depending on the empty lines in the HoT
		this.update_extra_yaxes();
		
		
		// update form fields to show according to type
		var list = document.querySelectorAll("#formulaire_editer_diagramme-"+this.chartId+" .updatable");
		var mypath, mytype, myvalue;
		var refresh_options = {data:{}};
		var chartType = this.chartType;
		
		// if we need to destroy and rebuild the chart (change of type, background color, etc.)
		/*if(refreshMode == "refresh"){
			this.update_datasets();
		}
		// if a simple update of the current chart is enough
		else{
			// if we switch the parsing method or change dataset attributes, we need to update the datasets as well
			if(refreshMode == 'redraw'){ 
				this.update_datasets();
			}
		}*/
		
		Array.prototype.forEach.call(list, child => {
			// test if property is allowed for this type of chart
			var use_attribute = true;
			// is this field restricted from this type of chart we don't use it and hide it
			if(child.closest('.no'+chartType) != null) use_attribute = false;
			// if this input is hidden for some other reasons we do not use it either
			if(child.closest('.hide') !== null) use_attribute = false;
			if(child.hasAttribute("data-path") && use_attribute){
				mypath = child.getAttribute("data-path").split(".");
				mytype = child.getAttribute("data-type");
				// deal with checkboxes: checked == true | unchecked == false
				if(child.type == 'checkbox'){ 
					if(child.checked) myvalue = true;
					else myvalue = false;
				}
				else myvalue = child.value;
				// force type of the value if specified, otherwise keep is as string
				if(typeof mytype !== "undefined"){
					if(mytype == "integer") myvalue = parseInt(myvalue);
					if(mytype == "float") myvalue = parseFloat(myvalue);
					if(mytype == "boolean")
						if(myvalue === "true") myvalue = true;
						else myvalue = false;
					if(mytype == "array") myvalue = myvalue.split("|");
					if(mytype == "mixed_string_array"){ 
						if(myvalue.indexOf("|") > -1) myvalue = myvalue.split("|");
					}
				}
				if(refreshMode == "refresh"){
					if(mypath.length >= 1){ 
						if(typeof refresh_options[mypath[0]] === "undefined"){ 
							if(mypath.length > 1 && !isNaN(mypath[1])) refresh_options[int_or_not(mypath[0])] = [];
							else refresh_options[int_or_not(mypath[0])] = {};
						}
					}
					if(mypath.length >= 2){ 
						if(typeof refresh_options[int_or_not(mypath[0])][int_or_not(mypath[1])] === "undefined"){ 
							if(mypath.length > 2 && !isNaN(mypath[2])) refresh_options[int_or_not(mypath[0])][int_or_not(mypath[1])] = [];
							else refresh_options[int_or_not(mypath[0])][int_or_not(mypath[1])] = {};
						}
					}
					if(mypath.length >= 3){ 
						if(typeof refresh_options[int_or_not(mypath[0])][int_or_not(mypath[1])][int_or_not(mypath[2])] === "undefined"){ 
							if(mypath.length > 3 && !isNaN(mypath[3])) refresh_options[int_or_not(mypath[0])][int_or_not(mypath[1])][int_or_not(mypath[2])] = [];
							else refresh_options[int_or_not(mypath[0])][int_or_not(mypath[1])][int_or_not(mypath[2])] = {};
						}
					}
					if(mypath.length >= 4){ 
						if(typeof refresh_options[int_or_not(mypath[0])][int_or_not(mypath[1])][int_or_not(mypath[2])][int_or_not(mypath[3])] === "undefined"){ 
							if(mypath.length > 4 && !isNaN(mypath[4])) refresh_options[int_or_not(mypath[0])][int_or_not(mypath[1])][int_or_not(mypath[2])][int_or_not(mypath[3])] = [];
							else refresh_options[int_or_not(mypath[0])][int_or_not(mypath[1])][int_or_not(mypath[2])][int_or_not(mypath[3])] = {};
						}
					}
					if(mypath.length >= 5){ 
						if(typeof refresh_options[int_or_not(mypath[0])][int_or_not(mypath[1])][int_or_not(mypath[2])][int_or_not(mypath[3])][int_or_not(mypath[4])] === "undefined"){
							if(mypath.length > 5 && !isNaN(mypath[5])) refresh_options[int_or_not(mypath[0])][int_or_not(mypath[1])][int_or_not(mypath[2])][int_or_not(mypath[3])][int_or_not(mypath[4])] = [];
							else refresh_options[int_or_not(mypath[0])][int_or_not(mypath[1])][int_or_not(mypath[2])][int_or_not(mypath[3])][int_or_not(mypath[4])] = {};
						}
					}
					if(mypath.length >= 6){ 
						if(typeof refresh_options[int_or_not(mypath[0])][int_or_not(mypath[1])][int_or_not(mypath[2])][int_or_not(mypath[3])][int_or_not(mypath[4])][int_or_not(mypath[5])] === "undefined"){
							if(mypath.length > 6 && !isNaN(mypath[6])) refresh_options[int_or_not(mypath[0])][int_or_not(mypath[1])][int_or_not(mypath[2])][int_or_not(mypath[3])][int_or_not(mypath[4])][int_or_not(mypath[5])] = [];
							refresh_options[int_or_not(mypath[0])][int_or_not(mypath[1])][int_or_not(mypath[2])][int_or_not(mypath[3])][int_or_not(mypath[4])][int_or_not(mypath[5])] = {};
						}
					}
					if(mypath.length == 1) refresh_options[int_or_not(mypath[0])] = myvalue;
					if(mypath.length == 2) refresh_options[int_or_not(mypath[0])][int_or_not(mypath[1])] = myvalue;
					if(mypath.length == 3) refresh_options[int_or_not(mypath[0])][int_or_not(mypath[1])][int_or_not(mypath[2])] = myvalue;
					if(mypath.length == 4) refresh_options[int_or_not(mypath[0])][int_or_not(mypath[1])][int_or_not(mypath[2])][int_or_not(mypath[3])] = myvalue;
					if(mypath.length == 5) refresh_options[int_or_not(mypath[0])][int_or_not(mypath[1])][int_or_not(mypath[2])][int_or_not(mypath[3])][int_or_not(mypath[4])] = myvalue;
					if(mypath.length == 6) refresh_options[int_or_not(mypath[0])][int_or_not(mypath[1])][int_or_not(mypath[2])][int_or_not(mypath[3])][int_or_not(mypath[4])][int_or_not(mypath[5])] = myvalue;
				}
				else{
					if(mypath.length == 6) this.chart[int_or_not(mypath[0])][int_or_not(mypath[1])][int_or_not(mypath[2])][int_or_not(mypath[3])][int_or_not(mypath[4])][int_or_not(mypath[5])] = myvalue;
					if(mypath.length == 5) this.chart[int_or_not(mypath[0])][int_or_not(mypath[1])][int_or_not(mypath[2])][int_or_not(mypath[3])][int_or_not(mypath[4])] = myvalue;/////
					if(mypath.length == 4) this.chart[int_or_not(mypath[0])][int_or_not(mypath[1])][int_or_not(mypath[2])][int_or_not(mypath[3])] = myvalue;
					if(mypath.length == 3) this.chart[int_or_not(mypath[0])][int_or_not(mypath[1])][int_or_not(mypath[2])] = myvalue;
					if(mypath.length == 2) this.chart[int_or_not(mypath[0])][int_or_not(mypath[1])] = myvalue;
					if(mypath.length == 1) this.chart[int_or_not(mypath[0])] = myvalue;
				}
			}
		});
		
		// if we need to destroy and rebuild the chart (change of type, background color, etc.)
		if(refreshMode == "refresh"){
			this.chartOptions = refresh_options;
			this.update_datasets();
			this.build("Prive");
			this.update_chart_units();
		}
		// if a simple update of the current chart is enough
		else{
			// if we switch the parsing method or change dataset attributes, we need to update the datasets as well
			if(refreshMode == 'redraw'){ 
				this.update_datasets();
				this.apply_datasets();
				this.update_chart_units();
			}
			this.chart.update();
		}
	}
	
	// update chartOptions datasets according to hot content and parsing method
	update_datasets(){
		// get datasets values from HoT
		this.get_datasets();
		// add colors to the chart
		this.insert_chart_colors();
		// get chart labels except bubble and scatter charts which have none
		if(this.chartType != "bubble" && this.chartType != "scatter") this.get_labels();
		else this.chartOptions.labels = {};
	}
	
	// copy datasets  from chartOptions into the chart datasets
	apply_datasets(){
		if(typeof this.chartOptions.data.labels !== 'undefined')  this.chart.data.labels = this.chartOptions.data.labels;
		this.chart.data.datasets = this.chartOptions.data.datasets;
	}
	
	get_labels(){
		var res = [];
		if(typeof this.hotData[0] !== 'undefined'){ 
			var start = 0;
			if(typeof this.hotData[0][0] !== 'undefined' && this.hotData[0][0] == null) start = 1;
			if(this.parsing == "columns"){
				for(i = start; i < this.hotData.length; i++){ 
					if(this.hotData[i][0] != null) res.push(this.hotData[i][0]); 
					else break;
				}	
			}
			else{
				for(var i = start; i < this.hotData[0].length; i++){ 
					if(this.hotData[0][i] != null) res.push(this.hotData[0][i]); 
					else break;
				}
			}
		}
		this.chartOptions.data.labels = res;
	}
	
	get_datasets(){
		var res = [], tmp = [], coord = [], startx = 0, starty = 0, naxes = 0, index_el = 0;
		// do we allow multiple y axis? We need to know which charts are compatible
		var multiaxis = false;
		if(["line", "bar", "horizontalBar", "bubble", "scatter"].includes(this.chartType)) multiaxis = true;
		// check if we have labels or not, depending on the chart type
		if(["polarArea", "pie", "doughnut"].includes(this.chartType)){ 
			if(this.parsing == "rows"){ startx = 0; starty = 1; }
			else{ startx = 1; starty = 0; }
		}
		else if(this.chartType == "scatter" || this.chartType == "bubble"){ 
			if(this.parsing == "rows"){ startx = 1; starty = 0; }
			else{ startx = 0; starty = 1; }
		}
		else{ startx = 1; starty = 1; }
		if(this.parsing == "columns"){
			if(starty == 1){ 
	  		index_el = 0;
	  		for(var i = startx; i < this.hotData[0].length; i++){ 
	  			if(this.hotData[0][i] != null){
		  			res[index_el] = {};
		  			if(this.chartType == "horizontalBar") res[index_el].xAxisID = "xaxis"+naxes;
		    		else res[index_el].yAxisID = "yaxis"+naxes;
		  			res[index_el].label = this.hotData[0][i];
		  			res[index_el].data = [];
			  		index_el++;
		  		}
		  		// if label null we assume it is another axis
		  		else{ 
		  			if(multiaxis) naxes++;
	    			else break;
					}
	  		}
			}
			naxes = 0;
			for(var i = starty; i < this.hotData.length; i++){
	    	index_el = 0;
	    	for(var j = startx; j < this.hotData[i].length; j++){
	    		if(this.hotData[i][j] != null){ 
	    			if(typeof res[index_el]  === "undefined") res[index_el] = {data : []};
	    			if(this.hotData[i][j].substr("|") != -1){
	    				coord = this.hotData[i][j].split("|");
	    				if(coord.length == 3) res[index_el].data.push({'x' : coord[0], 'x' : coord[1], 'r' : coord[2]});
	    				else if(coord.length == 2) res[index_el].data.push({'x' : coord[0], 'y' : coord[1]});
	    				else res[index_el].data.push(this.hotData[i][j]);
	    			}
	    			else res[index_el].data.push(this.hotData[i][j]);
	    			index_el++;
	    		}
	    		// if empty cell, stop parsing except for multiaxis compatible charts
	    		else{ 
	    			if(multiaxis) naxes++;
	    			else break;
	    		}
	    	}
			}
		}
		else{
			for(var i = starty; i < this.hotData.length; i++){
				if(typeof this.hotData[i] !== 'undefined' && typeof this.hotData[i][0] !== 'undefined' && this.hotData[i][0] != null){
		    	res[index_el] = {};
		    	if(multiaxis){ 
		    		if(this.chartType == "horizontalBar") res[index_el].xAxisID = "xaxis"+naxes;
		    		else res[index_el].yAxisID = "yaxis"+naxes;
		    	}
		    	tmp = [];
		    	if(startx == 1){
		    		res[index_el].label = this.hotData[i][0];
		    	}
		    	for(var j = startx; j < this.hotData[i].length; j++){
		    		if(this.hotData[i][j] != null){ 
		    			if(this.hotData[i][j].substr("|") != -1){
		    				coord = this.hotData[i][j].split("|");
		    				if(coord.length == 3) tmp.push({'x' : coord[0], 'y' : coord[1], 'r' : coord[2]});
		    				else if(coord.length == 2) tmp.push({'x' : coord[0], 'y' : coord[1]});
		    				else tmp.push(this.hotData[i][j]);
		    			}
		    			else tmp.push(this.hotData[i][j]);
		    		}
		    	}
		    	res[index_el].data = tmp;
		    	index_el++;
		    }
		    // looks like an empty row (we only test first value)
		    else{ 
		    	// switch to next y axis if chart can handle it
		    	if(multiaxis) naxes ++;
		    	// otherwise we stop parsing there is no reason to have empty cells in between
		    	else break;
				}
			}
		}
		this.chartOptions.data.datasets = res;
	}
	
	count_extra_yaxes(){
		var startx = 0, starty = 0, naxes = 0, null_found_before = false;
		// do we allow multiple x/y axis? We need to know which charts are compatible
		var multiaxes = false;
		if(["line", "bar", "horizontalBar", "bubble", "scatter"].includes(this.chartType)) multiaxes = true;
		// no need to go further if chart not compatible
		if(!multiaxes) return 0;
		// check if we have labels or not, depending on the chart type
		if(this.chartType == "scatter" || this.chartType == "bubble"){ 
			if(this.parsing == "rows"){ startx = 1; starty = 0; }
			else{ startx = 0; starty = 1; }
		}
		else{ startx = 1; starty = 1; }
		if(this.parsing == "columns"){
			for(var i = startx; i < this.hotData[0].length; i++){ 
				if(typeof this.hotData[i] === 'undefined' || typeof this.hotData[i][0] === 'undefined') break;
				if(this.hotData[0][i] != null){ 
					if(null_found_before) naxes++;
					null_found_before = false;
				}
				else null_found_before = true;
			}
	  }		
		else{
			for(var i = starty; i < this.hotData.length; i++){
				if(typeof this.hotData[i][0] === 'undefined') break;
				if(this.hotData[i][0] != null){ 
					if(null_found_before) naxes++;
					null_found_before = false;
				}
				else null_found_before = true;
			}
		}
		return naxes;
	}
	
	update_extra_yaxes(){
		var n_axes = this.count_extra_yaxes();
		// first remove all unnecessary Y axis already in form
		var axes_there = 0;
		var extra_list = document.querySelectorAll("#formulaire_editer_diagramme-"+this.chartId+" form .extra-axis");
		var keep_existing_extra_axes = true;
		// if we switched to horizontalBars and there are extra Y axes, we do not keep them
		if(this.chartType == "horizontalBar" && document.querySelector("#formulaire_editer_diagramme-"+this.chartId+" form .extra-axis-y") != null) keep_existing_extra_axes = false;
		// same thing if we switch to another type of chart with existing extra X axes 
		if(this.chartType != "horizontalBar" && document.querySelector("#formulaire_editer_diagramme-"+this.chartId+" form .extra-axis-x") != null) keep_existing_extra_axes = false;
		[].forEach.call(extra_list, function(el, index){
		  if(index >= n_axes || !keep_existing_extra_axes){ 
		  	el.parentNode.removeChild(el);
		  }
		  // if we keep the existing axis we increment the axis index for potential additions
		  else axes_there++;
		});
		// add extra axes if necessary
		var model_content = document.getElementById("extra-axes-model").innerHTML;
		for(var i = axes_there; i < n_axes; i++){
			// we insert axes with index starting 1 because index 0 is taken by default X and Y axes
			var model_content_new = model_content.replace(/xxx/g, (i+1));
			// for horizontalbars we have multiple X axes, not Y
			if(this.chartType == "horizontalBar"){ 
				model_content_new = model_content_new.replace(/zzz/g, "x");
				model_content_new = model_content_new.replace(/ZZZ/g, "X");
			}
			else{
				model_content_new = model_content_new.replace(/zzz/g, "y");
				model_content_new = model_content_new.replace(/ZZZ/g, "Y");
			}
			document.querySelector("#formulaire_editer_diagramme-"+this.chartId+" form .chart-axis .inner-box .extra-axis-container").insertAdjacentHTML('beforeend', model_content_new);
			var list = document.querySelectorAll("#formulaire_editer_diagramme-"+this.chartId+" .extra-axis-"+(i+1));
			Array.prototype.forEach.call(list, child => {
				child.addEventListener("change", function(){refresh_chartel(this);} );
			});
		}
	}
	
	// set handsontable events that update the chart 
	set_hooks(){
		var hooks = Handsontable.hooks.getRegistered();
		var config = this.hotConfig;
		var chartId = this.chartId;
		hooks.forEach(function(hook) {
	  	if(hook === 'afterChange' || hook === 'afterCreateRow' || hook === 'afterRemoveRow' || hook === 'afterCreateCol' || hook === 'afterRemoveCol') {
	    	config[hook] = function() {
		    	if(typeof window["spipChartPrive"+chartId] !== 'undefined'){
						var hotData = window["spipChartPrive"+chartId].hot.getSourceDataArray();
	        	// first we replace all empty strings with null as it messes with other features
						var updated = false;
						for(var i = 0; i < hotData.length; i++){
							for(var j = 0; j < hotData[i].length; j++){
								if(typeof hotData[i][j] == "string" && hotData[i][j].trim() == ''){ 
									hotData[i][j] = null;
									updated = true;
								}
							}
						}
						if(updated) window["spipChartPrive"+chartId].hot.loadData(hotData);
						if(hook == 'afterChange') window["spipChartPrive"+chartId].refresh_chart("redraw", false);
						else window["spipChartPrive"+chartId].refresh_chart("refresh", false);
					}	    	
		  	}
	  	}
		});
	}
	
	beforeSubmitChart(){
		this.setHotValue();
		this.setChartThumb();
		return true; 
	}
	
	setChartThumb(){
		var canvas = document.getElementsByClassName("chartJSPriveContainer"+this.chartId)[0];
		var imgData = canvas.toDataURL('image/jpeg');
	  if(document.getElementById("spip_thumb"+this.chartId) != null) document.getElementById("spip_thumb"+this.chartId).value = imgData;
	}
	
	setHotValue(){
		if(document.getElementById("json_hotdata" + this.chartId) != null) document.getElementById("json_hotdata"+this.chartId).value = JSON.stringify(this.hot.getSourceData());
	}	
	
}

function int_or_not(string){
	if(isNaN(string)) return string;
	return parseInt(string);
}

function export_csv(el, chartId){
	var newHotData = window["spipChartPrive"+chartId].hot.getSourceData();
	var lineArray = [];
	newHotData.forEach(function (infoArray, index) {
	    var line = infoArray.join(",");
	    lineArray.push(line);
	});
	var csvContent = lineArray.join("\n");
	el.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(csvContent));
}

// Confirm browser supports HTML5 File API
var browserSupportFileUpload = function() {
	var isCompatible = false;
	if(window.File && window.FileReader && window.FileList && window.Blob) {
		isCompatible = true;
	}
	return isCompatible;
};

// Upload selected file and create array
var uploadCsvFile = function(evt) {
	var file = evt.target.files[0];
	var chartId = evt.target.getAttribute("data-id");
	var reader = new FileReader();
	reader.readAsText(file);
	reader.onload = function(event) {
		var csv_import = Papa.parse(event.target.result).data;
		// stupid papaparse set null values to ""
		for(var i = 0; i < csv_import.length; i++){
			for(var j = 0; j < csv_import[i].length; j++){ 
				csv_import[i][j] = csv_import[i][j].replace(/\r\n|\r|\n/g, "");
				if(csv_import[i][j] == "") csv_import[i][j] = null;
			}
		}
		window["spipChartPrive"+chartId].hot.loadData(csv_import);
		window["spipChartPrive"+chartId].refresh_chart();
		prepare_upload_link(chartId);
	};
};

function prepare_upload_link(chartId){
	// event listener for file upload
	var upload_element = document.getElementById('file-input'+chartId);
	if(browserSupportFileUpload()){
		upload_element.addEventListener('change', uploadCsvFile, false);
	}else{
		// hide link
		upload_element.parentElement.classList.add("hide");
	}
}

// if field changed we update the chart accordinlgy
function refresh_chartel(el){
	chartId = el.closest(".formulaire_diagramme").getAttribute("data-id");
	if(el.classList.contains("refresh")){ 
		if(el.name == "json_type") window["spipChartPrive"+chartId].refresh_chart("refresh", true);
		else window["spipChartPrive"+chartId].refresh_chart("refresh", false);
	}
	else if(el.classList.contains("redraw")) window["spipChartPrive"+chartId].refresh_chart("redraw", false);
	else window["spipChartPrive"+chartId].refresh_chart("update", false);
}