<?php
// set global var for our chart ids
function diagrammes_set_global($value, $name){
	if(is_array($value)){ 
		if(!isset($GLOBALS[$name]) || !is_array($GLOBALS[$name])) $GLOBALS[$name] = $value;
		else $GLOBALS[$name] = array_merge($GLOBALS[$name], $value);
	}
	else $GLOBALS[$name] = $value;
}

function diagrammes_get_global($name){
	if(isset($GLOBALS[$name])) return $GLOBALS[$name];
	return null;
}

function diagrammes_test_global($name, $value = null){
	if(isset($GLOBALS[$name])){ 
		if(is_array($GLOBALS[$name])){
			if(in_array($value, $GLOBALS[$name])) return true;
		}
		else return true;
	}
	return false;
}

// conversion de couleur
function diagrammes_hex2rgb($hex) {
	   $hex = str_replace("#", "", $hex);

	   if(strlen($hex) == 3) {
	      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
	      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
	      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
	   } else {
	      $r = hexdec(substr($hex,0,2));
	      $g = hexdec(substr($hex,2,2));
	      $b = hexdec(substr($hex,4,2));
	   }

	   $rgb = array($r, $g, $b);
	   return implode(",", $rgb); // returns the rgb values separated by commas
}

// print a php array into a javascript array
function diagrammes_print_js_array($array, $with_keys = false){
	$res = "[";
	foreach($array as $key => $value){
		if($with_keys){
			if(is_int($key)) $res .= $key.' => ';
			else $res .= '"'.$key.'" => ';
		}
		$res .= $value.', ';
	}
	if(substr($res, -2) == ", ") $res = substr($res, 0 -2);
	$res .= "]";
	return $res;
}

function diagrammes_get_chartdata($fichier){
	return $json_content = json_decode(file_get_contents(get_spip_doc($fichier)), true);
}

function diagrammes_get_labels($arr, $parsing){
	$res = array();
	if(isset($arr[0])){ 
		$start = 0;
		if(!isset($arr[0][0]) || (isset($arr[0][0]) && trim($arr[0][0]) == '')) $start = 1;
		if($parsing == "columns"){
			for($i = $start; $i < sizeof($arr); $i++){ 
				if(trim($arr[$i][0]) != '') array_push($res, $arr[$i][0]); 
				else break;
			}	
		}
		else{
			for($i = $start; $i < sizeof($arr[0]); $i++){ 
				if(trim($arr[0][$i]) != '') array_push($res, $arr[0][$i]); 
				else break;
			}
		}
	}
	return $res;
}

function diagrammes_get_datasets($arr, $parsing, $chart_type){
	$res = array();
	$naxes = 0;
	// do we allow multiple y axis? We need to know which charts are compatible
	$multiaxis = false;
	if(in_array($chart_type, ["line", "bar", "horizontalBar", "bubble", "scatter"])) $multiaxis = true;
	// check if we have labels or not, depending on the chart type
	if($chart_type == "polarArea" || $chart_type == "pie" || $chart_type == "doughnut"){ 
		if($parsing == "rows"){ $startx = 0; $starty = 1; }
		else{ $startx = 1; $starty = 0; }
	}
	elseif($chart_type == "scatter" || $chart_type == "bubble"){ 
		if($parsing == "rows"){ $startx = 1; $starty = 0; }
		else{ $startx = 0; $starty = 1; }
	}
	else{ $startx = 1; $starty = 1; }
	if($parsing == "columns"){
		if($starty == 1){ 
  		$index_el = 0;
  		for($i = $startx; $i < sizeof($arr[0]); $i++){ 
  			if(trim($arr[0][$i]) != ''){
	  			$res[$index_el] = new stdClass();
	  			if($chart_type == "horizontalBar") $res[$index_el]->xAxisID = "xaxis".$naxes;
	    		else $res[$index_el]->yAxisID = "yaxis".$naxes;
	  			$res[$index_el]->label = $arr[0][$i];
	  			$res[$index_el]->data = array();
	  			$index_el++;
	  		}
	  		// if label null we assume it is another axis
	  		else{ 
	  			if($multiaxis) $naxes++;
    			else break;
				}
  		}
		}
		$naxes = 0;
		for($i = $starty; $i < sizeof($arr); $i++){
    	$index_el = 0;
    	for($j = $startx; $j < sizeof($arr[$i]); $j++){
    		if(trim($arr[$i][$j]) != ''){ 
    			if(!isset($res[$index_el])){ 
    				$res[$index_el] = new stdClass();
    				$res[$index_el]->data = array();
    			}
    			if(strpos($arr[$i][$j], "|") !== false){
    				$coord = explode("|", $arr[$i][$j]);
    				if(sizeof($coord) == 3){ 
    					$tmp = new stdClass();
    					$tmp->x = $coord[0];
    					$tmp->y = $coord[1];
    					$tmp->r = $coord[2];
    				}
    				elseif(sizeof($coord) == 2){
    					$tmp = new stdClass();
    					$tmp->x = $coord[0];
    					$tmp->y = $coord[1];
    				}
    				else $tmp = $arr[$i][$j];
    				array_push($res[$index_el]->data, $tmp);
    			}
    			else array_push($res[$index_el]->data, $arr[$i][$j]);
    			$index_el++;
    		}
    		// if empty cell, stop parsing except for multiaxis compatible charts
    		else{ 
    			if($multiaxis) $naxes++;
    			else break;
    		}
    	}
		}
	}
	else{
		$index_el = 0;
		for($i = $starty; $i < sizeof($arr); $i++){
			if(isset($arr[$i]) && isset($arr[$i][0]) && trim($arr[$i][0]) != ''){
	    	$res[$index_el] = new stdClass();
	    	if($multiaxis){ 
	    		if($chart_type == "horizontalBar") $res[$index_el]->xAxisID = "xaxis".$naxes;
	    		else $res[$index_el]->yAxisID = "yaxis".$naxes;
	    	}
	    	$tmp = array();
	    	if($startx == 1){
	    		$res[$index_el]->label = $arr[$i][0];
	    	}
	    	for($j = $startx; $j < sizeof($arr[$i]); $j++){
	    		if(trim($arr[$i][$j]) != ''){ 
	    			if(strpos($arr[$i][$j], "|") !== false){
	    				$coord = explode("|", $arr[$i][$j]);
	    				if(sizeof($coord) == 3){ 
	    					$tmp_object = new stdClass();
	    					$tmp_object->x = $coord[0];
	    					$tmp_object->y = $coord[1];
	    					$tmp_object->r = $coord[2];
	    				}
	    				elseif(sizeof($coord) == 2){
	    					$tmp_object = new stdClass();
	    					$tmp_object->x = $coord[0];
	    					$tmp_object->y = $coord[1];
	    				}
	    				else $tmp_object = $arr[$i][$j];
	    				array_push($tmp, $tmp_object);
	    			}
	    			else array_push($tmp, $arr[$i][$j]);
	    		}
	    	}
	    	$res[$index_el]->data = $tmp;
	    	$index_el++;
	    }
	    // looks like an empty row (we only test first value)
	    else{ 
	    	// switch to next y axis if chart can handle it
	    	if($multiaxis) $naxes ++;
	    	// otherwise we stop parsing there is no reason to have empty cells in between
	    	else break;
			}
		}
	}
	return $res;
}
?>