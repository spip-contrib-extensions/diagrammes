<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

function diagrammes_header_prive_css($flux){
  $flux .=
  	 			'<link rel="stylesheet" type="text/css" href="'.find_in_path("css/handsontable.full.min.css").'" />'
  				.'<link rel="stylesheet" type="text/css" href="'.find_in_path("css/chartjs_prive.css").'" />';
  return $flux;
}

function diagrammes_insert_head($flux){
  $flux .= 
         "<script type='text/javascript' src='".find_in_path('js/chartjs_public.js')."'></script>"
         . "<script type='text/javascript' src='".find_in_path('js/Chart.js')."'></script>"
         . "<script type='text/javascript' src='".find_in_path('js/chartjs-plugin-datalabels.min.js')."'></script>";
  if(lire_config('chartjs/deferred') == 'yes') $flux .= "<script type='text/javascript' src='".find_in_path('js/chartjs-plugin-deferred.js')."'></script>";
  return $flux;
}

function diagrammes_header_prive($flux){
  $flux .= 
         "<script type='text/javascript' src='".find_in_path('js/Chart.js')."'></script>"
         . "<script type='text/javascript' src='".find_in_path('js/handsontable.full.min.js')."'></script>"
         . "<script type='text/javascript' src='".find_in_path('js/chartjs_public.js')."'></script>"
         . "<script type='text/javascript' src='".find_in_path('js/chartjs_prive.js')."'></script>"
         . "<script type='text/javascript' src='".find_in_path('js/papaparse.js')."'></script>"
         . "<script type='text/javascript' src='".find_in_path('js/chartjsform.js')."'></script>"
         . "<script type='text/javascript' src='".find_in_path('js/chartjs-plugin-datalabels.min.js')."'></script>";
  if(lire_config('chartjs/deferred') == 'yes') $flux .= "<script type='text/javascript' src='".find_in_path('js/chartjs-plugin-deferred.js')."'></script>";
  return $flux;
}

function diagrammes_document_desc_actions($flux){
	$row = sql_getfetsel('id_document', 'spip_documents', 'media="diagramme" and id_document=' . intval($flux['args']['id_document']));
	if($row){
		//$flux['data'] .= '<span class="edit-chart"><a href="'.generer_url_ecrire('diagramme_edit','id_document='.$flux['args']['id_document']).'" target="_blank" class="editbox" title="'._T("diagrammes:editer_chart").'" tabindex="0" role="button"><img src="'.chemin_image('edit-16.png').'" alt="'._T("diagrammes:editer_chart").'" width="16" height="16" style="background-image: url(\''.chemin_image('diagrammes-16.png').'\');"></a></span>';
		$flux['data'] .= '<span class="edit-chart"><a href="'.generer_url_ecrire('diagramme_edit','id_document='.$flux['args']['id_document']).'" target="_blank" class="editbox" title="'._T("diagrammes:editer_chart").'" tabindex="0" role="button">'._T("diagrammes:editer_chart").'</a></span>';
	}
	return $flux;
}

?>